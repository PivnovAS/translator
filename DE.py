import Dictionary as Dict
D = Dict.Dictionary()


class DE:  # Language of developer
    def __init__(self):
        self.text = ''
        self.color_text = ''
        self.words = []
        self.A_Words = []

    def set_text(self, text):
        self.text = text
        self.words = text.split()

    def set_color_text(self, text):
        self.color_text = text

    def get_text(self):
        return self.text

    def get_color_text(self):
        return self.color_text

    class Pronoun:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.Amount = ''
            self.Voice = ''
            self.Person = ''

            self.Part_Speech = 'Pronoun'
            self.Part_Sentence = ''

            self.Dict_Index = 0

    class Noun:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix1 = ''

            self.state = ''
            self.Amount = ''
            self.Voice = ''

            self.Part_Speech = 'Noun'
            self.Part_Sentence = ''

            self.Dict_Index = 0

    class Verb:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.Time = ''
            self.complexity = ''
            self.Modal_Verb = ''

            self.Part_Speech = 'Verb'
            self.Part_Sentence = 'Predicate'

            self.Dict_Index = 0
            self.Dict_Index_Modal_Verb = 0

    class Preposition:
        def __init(self):
            self.Word = ''

            self.Part_Speech = 'Preposition'
            self.Part_Sentence = 'Preposition'

            self.Dict_Index = 0

    def parse(self):
        self.A_Words.clear()
        verb_lever = True
        for i in range(len(self.words)):
            for j in range(1, len(D.DE_Pronoun)):
                Register = Dict.check_register(self.words[i])
                amount = ["Singular", "Plural"]
                temp = ''
                for s in amount:
                    if (j == 3) and (s == 'Plural'):
                        temp = 'not'
                    if (j == 4) and (s == 'Plural'):
                        temp = 'not'

                    if temp != 'not':
                        if self.words[i] == D.get_DE_Pronoun(j, s, Register):
                            self.A_Words.append(self.Pronoun())
                            Current_Word = len(self.A_Words) - 1
                            self.A_Words[Current_Word].Root = D.get_DE_Pronoun(j, s, Register)
                            self.A_Words[Current_Word].Word = D.get_DE_Pronoun(j, s, Register)
                            self.A_Words[Current_Word].Part_Sentence = 'Subject'
                            self.A_Words[Current_Word].Voice = 'Active'
                            self.A_Words[Current_Word].Amount = s
                            self.A_Words[Current_Word].Person = j
                            self.A_Words[Current_Word].Dict_Index = None


            for j in range(len(D.DE_Noun)):
                if D.get_state_DE_Noun(j) == 'Verbal':
                    Register = Dict.check_register(self.words[i])
                    if self.words[i] == D.get_DE_Noun(j, Register):
                        self.A_Words.append(self.Noun())
                        Current_Word = len(self.A_Words) - 1
                        self.A_Words[Current_Word].Root = D.get_DE_Noun(j, Register)
                        self.A_Words[Current_Word].Word = D.get_DE_Noun(j, Register)
                        self.A_Words[Current_Word].state = 'Verbal'
                        self.A_Words[Current_Word].Amount = 'Singular'
                        if Current_Word == 0:
                            self.A_Words[Current_Word].Voice = 'Subject'
                            self.A_Words[Current_Word].Part_Sentence = 'Subject'
                        else:
                            self.A_Words[Current_Word].Voice = 'Object'
                            self.A_Words[Current_Word].Part_Sentence = 'Object'
                        self.A_Words[Current_Word].Dict_Index = j

                    if verb_lever == 1:
                        verb_suffix = ['e', 'est', 'et', 'en']
                        for suffix in verb_suffix:
                            word = D.get_DE_Noun(j, Register)
                            word = Dict.del_suffix(word, 'ung')
                            if self.words[i] == word + suffix:
                                self.A_Words.append(self.Verb())
                                Current_Word = len(self.A_Words) - 1
                                self.A_Words[Current_Word].Root = word
                                self.A_Words[Current_Word].Word = word + suffix
                                self.A_Words[Current_Word].Suffix = suffix
                                self.A_Words[Current_Word].Dict_Index = j
                                self.A_Words[Current_Word].Part_Speech = 'Verb'
                                self.A_Words[Current_Word].Part_Sentence = 'Predicate'
                                if suffix == 'et':
                                    self.A_Words[Current_Word].Time = 'Past'
                                else:
                                    self.A_Words[Current_Word].Time = 'Present'

                    future_verb = ['werde', 'wirst', 'wird', 'werden', 'werdet']
                    for verb in future_verb:
                        if self.words[i] == verb:
                            word = D.get_DE_Noun(j, Register)
                            word = Dict.del_suffix(word, 'ung')
                            suffix = 'en'
                            if self.words[i+1] == word + suffix:
                                self.A_Words.append(self.Verb())
                                Current_Word = len(self.A_Words) - 1
                                self.A_Words[Current_Word].Root = word
                                self.A_Words[Current_Word].Word = word + suffix
                                self.A_Words[Current_Word].Suffix = suffix
                                self.A_Words[Current_Word].Dict_Index = j
                                self.A_Words[Current_Word].Part_Speech = 'Verb'
                                self.A_Words[Current_Word].Modal_Verb = verb
                                self.A_Words[Current_Word].Time = 'Future'
                                verb_lever = False

                if D.get_state_DE_Noun(j) == 'Object':
                    Register = Dict.check_register(self.words[i])
                    if self.words[i] == D.get_DE_Noun(j, Register):
                        self.A_Words.append(self.Noun())
                        Current_Word = len(self.A_Words) - 1
                        self.A_Words[Current_Word].Root = D.get_DE_Noun(j, Register)
                        self.A_Words[Current_Word].Word = D.get_DE_Noun(j, Register)
                        self.A_Words[Current_Word].state = 'Object'
                        self.A_Words[Current_Word].Amount = 'Singular'
                        if Current_Word == 0:
                            self.A_Words[Current_Word].Voice = 'Subject'
                            self.A_Words[Current_Word].Part_Sentence = 'Subject'
                        else:
                            self.A_Words[Current_Word].Voice = 'Object'
                            self.A_Words[Current_Word].Part_Sentence = 'Object'
                        self.A_Words[Current_Word].Dict_Index = j

            for j in range(len(D.DE_Preposition)):
                if self.words[i] == D.get_DE_Preposition(j):
                    self.A_Words.append(self.Preposition())
                    Current_Word = len(self.A_Words) - 1
                    self.A_Words[Current_Word].Part_Sentence = 'Preposition'
                    self.A_Words[Current_Word].Part_Speech = 'Preposition'
                    self.A_Words[Current_Word].Word = D.get_DE_Preposition(j)
                    self.A_Words[Current_Word].Dict_Index = j

    def show(self):
        for i in self.A_Words:
            if i.Part_Speech == 'Pronoun':
                print('Word:             ', i.Word)
                print('Person:           ', i.Person)
                print('Part of speech:   ', i.Part_Speech)
                print('Part of Sentence: ', i.Part_Sentence)
                print('Root:             ', i.Root)
                print('Suffix:           ', i.Suffix)
                print('Amount:           ', i.Amount)
                print('Voice:            ', i.Voice)
                print('Index:            ', i.Dict_Index)
                print()

            if i.Part_Speech == 'Noun':
                print('Word:           ', i.Word)
                print('Part of speech: ', i.Part_Speech)
                print('Root:           ', i.Root)
                print('Suffix1:        ', i.Suffix1)
                print('Amount:         ', i.Amount)
                print('Voice:          ', i.Voice)
                print('State:          ', i.state)
                print('Index:          ', i.Dict_Index)
                print('Part_Sentence:  ', i.Part_Sentence)
                print()

            if i.Part_Speech == 'Adjective':
                print('Word:           ', i.Word)
                print('Part of speech: ', i.Part_Speech)
                print('Root:           ', i.Root)
                print('Suffix:         ', i.Suffix)
                print('State:          ', i.state)
                print('Index:          ', i.Dict_Index)
                print()

            if i.Part_Speech == 'Verb':
                print('Word:           ', i.Word)
                print('Part of speech: ', i.Part_Speech)
                print('Root:           ', i.Root)
                print('Suffix:         ', i.Suffix)
                print('Time:           ', i.Time)
                print('Index:          ', i.Dict_Index)
                print('Future Verb:    ', i.Modal_Verb)
                print()

            if i.Part_Speech == 'Preposition':
                print('Word:            ', i.Word)
                print('Part of speech:  ', i.Part_Speech)
                print('Part of sentence:', i.Part_Sentence)
                print('Index:           ', i.Dict_Index)
                print()
