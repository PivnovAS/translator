import Dictionary as Dict
D = Dict.Dictionary()


class English:
    def __init__(self):
        self.text = ''
        self.color_text = ''
        self.words = []
        self.A_Words = []

    def set_text(self, text):
        self.text = text
        self.words = text.split()

    def set_color_text(self, text):
        self.color_text = text

    def get_text(self):
        return self.text

    def get_color_text(self):
        return self.color_text

    class Pronoun:
        def __init__(self):
            self.Word = ''
            self.Root = ''

            self.Amount = ''
            self.Voice = ''
            self.Person = ''

            self.Part_Speech = 'Pronoun'
            self.Part_Sentence = ''

    class Noun:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.Amount = ''
            self.Voice = ''

            self.Part_Speech = 'Noun'
            self.Part_Sentence = ''

            self.Dict_Index = 0

    class Adjective:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.state = ''

            self.Part_Speech = 'Adjective'
            self.Part_Sentence = ''
            self.Dict_Index = 0

    class Verb:
        def __init__(self):
            self.Word = ''
            self.Root = ''
            self.Suffix = ''

            self.Time = ''

            self.Part_Speech = ''
            self.Complexity = ''
            self.Modal_Verb = ''
            self.Future_Verb = ''
            self.TO_Modal_Verb = ''
            self.TO_Future_Time = ''

            self.Construct = ''

            self.Part_Speech = 'Verb'
            self.Part_Sentence = 'Predicate'

            self.Dict_Index = 0
            self.Dict_Index_Modal_Verb = 0

    class Preposition:
        def __init(self):
            self.Word = ''

            self.Part_Speech = 'Preposition'
            self.Part_Sentence = 'Preposition'

            self.Dict_Index = 0

    def show(self):
        for i in self.A_Words:
            if i.Part_Speech == 'Pronoun':
                print('Word:             ', i.Word)
                print('Person:           ', i.Person)
                print('Part of speech:   ', i.Part_Speech)
                print('Root:             ', i.Root)
                print('Amount:           ', i.Amount)
                print('Voice:            ', i.Voice)
                print()

            if i.Part_Speech == 'Noun':
                print('Word:            ', i.Word)
                print('Part of speech:  ', i.Part_Speech)
                print('Root:            ', i.Root)
                print('Suffix:          ', i.Suffix)
                print('Amount:          ', i.Amount)
                print('Voice:           ', i.Voice)
                print('Part of sentence:', i.Part_Sentence)
                print()

            if i.Part_Speech == 'Adjective':
                print('Word:           ', i.Word)
                print('Part of speech: ', i.Part_Speech)
                print('Root:           ', i.Root)
                print('Suffix:         ', i.Suffix)
                print('State:          ', i.state)
                print()

            if i.Part_Speech == 'Verb':
                print('Word:           ', i.Word)
                print('Part of speech: ', i.Part_Speech)
                print('Future_Verb:    ', i.Future_Verb)
                print('Modal Verb:     ', i.Modal_Verb)
                print('Root:           ', i.Root)
                print('Suffix:         ', i.Suffix)
                print('Time:           ', i.Time)
                print()

            if i.Part_Speech == 'Preposition':
                print('Word:            ', i.Word)
                print('Part of speech:  ', i.Part_Speech)
                print('Part of sentence:', i.Part_Sentence)
                print('Index:           ', i.Dict_Index)
                print()

# test = English()
# test.set_text('I created works')
# print(test.get_text())
# test.show()
