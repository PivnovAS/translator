import EN as ENG
import DE as DEU
import FR as FRP
import Dictionary as Dict

D = Dict.Dictionary()


class Translator:
    def __init__(self):
        self.EN = ENG.English()
        self.DE = DEU.DE()
        self.FR = FRP.France()

    def set_text(self, text):
        self.DE.set_text(text)
        self.DE.parse()

    def get_text_DE(self):
        return self.DE.get_text()

    def get_text_EN(self):
        return self.EN.get_text()

    def get_text_FR(self):
        return self.FR.get_text()

    def get_color_text_EN(self):
        return self.EN.get_color_text()

    def get_color_text_FR(self):
        return self.FR.get_color_text()

    def translateDEtoEN(self):
        self.EN.A_Words.clear()
        self.create1()

        words = []
        for word in self.EN.A_Words:
            words.append(word.Word)

        text_EN = ' '.join(words)

        self.EN.set_text(text_EN)


    def create1(self):
        for i in range(len(self.DE.A_Words)):
            if self.DE.A_Words[i].Part_Speech == 'Pronoun':
                temp = self.EN.Pronoun()
                temp.Amount = self.DE.A_Words[i].Amount
                temp.Person = self.DE.A_Words[i].Person
                temp.Root = D.get_EN_Pronoun(temp.Person, temp.Amount, 'Active', 1)
                temp.Word = D.get_EN_Pronoun(temp.Person, temp.Amount, 'Active', 1)
                temp.Voice = 'Active'
                self.EN.A_Words.append(temp)

            if self.DE.A_Words[i].Part_Speech == 'Noun':
                temp = self.EN.Noun()
                temp.Amount = self.DE.A_Words[i].Amount
                temp.Dict_Index = self.DE.A_Words[i].Dict_Index
                temp.Root = D.get_EN_Noun(temp.Dict_Index, 1)
                temp.Word = D.get_EN_Noun(temp.Dict_Index, 1)
                temp.Voice = 'Active'
                temp.Part_Sentence = self.DE.A_Words[i].Part_Sentence
                self.EN.A_Words.append(temp)

            if self.DE.A_Words[i].Part_Speech == 'Verb':
                temp = self.EN.Verb()
                if self.DE.A_Words[i].Time != 'Future':
                    temp.Time = self.DE.A_Words[i].Time
                    temp.Dict_Index = self.DE.A_Words[i].Dict_Index
                    temp.Root = D.get_EN_Noun(temp.Dict_Index, 0)
                    root = temp.Root
                    noun_suffix = ['ition', 'ion', 'ure']
                    for s in noun_suffix:
                        root = Dict.del_suffix(root, s)
                    temp.Root = root
                    suffix = ''
                    if temp.Time == 'Past':
                        suffix = 'ed'
                    if temp.Time == 'Present':
                        if self.DE.A_Words[0].Part_Speech == 'Pronoun':
                            if self.DE.A_Words[0].Person > 2:
                                suffix = 's'
                            else:
                                suffix = ''
                        if self.DE.A_Words[0].Part_Speech == 'Noun':
                            suffix = 's'

                    temp.Word = root + suffix
                    temp.Part_Sentence = self.DE.A_Words[i].Part_Sentence
                    self.EN.A_Words.append(temp)

                if self.DE.A_Words[i].Time == 'Future':
                    temp.Time = self.DE.A_Words[i].Time
                    temp.Dict_Index = self.DE.A_Words[i].Dict_Index
                    temp.Root = D.get_EN_Noun(temp.Dict_Index, 0)
                    root = temp.Root
                    noun_suffix = ['ition', 'ion', 'ure']
                    for s in noun_suffix:
                        root = Dict.del_suffix(root, s)
                    temp.Root = root
                    future_verb = 'will'
                    temp.Word = future_verb + ' ' + root
                    temp.Future_Verb = future_verb
                    temp.Part_Sentence = self.DE.A_Words[i].Part_Sentence
                    self.EN.A_Words.append(temp)


    def translateENtoFR(self):
        self.FR.A_Words.clear()
        self.create2()

        words = []
        for word in self.FR.A_Words:
            words.append(word.Word)

        text_FR = ' '.join(words)

        self.FR.set_text(text_FR)


    def create2(self):
        for i in range(len(self.DE.A_Words)):
            if self.DE.A_Words[i].Part_Speech == 'Pronoun':
                temp = self.FR.Pronoun()
                temp.Amount = self.DE.A_Words[i].Amount
                temp.Person = self.DE.A_Words[i].Person
                temp.Root = D.get_FR_Pronoun(temp.Person, temp.Amount, 1)
                temp.Word = D.get_FR_Pronoun(temp.Person, temp.Amount, 1)
                temp.Voice = 'Active'
                self.FR.A_Words.append(temp)

            if self.DE.A_Words[i].Part_Speech == 'Noun':
                temp = self.FR.Noun()
                temp.Amount = self.DE.A_Words[i].Amount
                temp.Dict_Index = self.DE.A_Words[i].Dict_Index
                temp.Root = D.get_FR_Noun(temp.Dict_Index, 1)
                temp.Word = D.get_FR_Noun(temp.Dict_Index, 1)
                temp.Voice = 'Active'
                temp.Part_Sentence = self.DE.A_Words[i].Part_Sentence
                self.FR.A_Words.append(temp)

            if self.DE.A_Words[i].Part_Speech == 'Verb':
                temp = self.FR.Verb()
                temp.Time = self.DE.A_Words[i].Time
                temp.Dict_Index = self.DE.A_Words[i].Dict_Index
                temp.Root = D.get_FR_Verb(temp.Dict_Index)
                suffix = ''
                if self.FR.A_Words[0].Part_Speech == 'Pronoun':
                    if temp.Time == 'Present':
                        if self.FR.A_Words[0].Person == 1 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'e'
                        if self.FR.A_Words[0].Person == 2 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'es'
                        if self.FR.A_Words[0].Person > 2 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'e'
                        if self.FR.A_Words[0].Person == 1 and self.FR.A_Words[0].Amount == 'Plural':
                            suffix = 'ons'
                        if self.FR.A_Words[0].Person == 2 and self.FR.A_Words[0].Amount == 'Plural':
                            suffix = 'ez'
                    if temp.Time == 'Past':
                        if self.FR.A_Words[0].Person == 1 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'ai'
                        if self.FR.A_Words[0].Person == 2 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'as'
                        if self.FR.A_Words[0].Person > 2 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'a'
                        if self.FR.A_Words[0].Person == 1 and self.FR.A_Words[0].Amount == 'Plural':
                            suffix = 'ames'
                        if self.FR.A_Words[0].Person == 2 and self.FR.A_Words[0].Amount == 'Plural':
                            suffix = 'ates'
                    if temp.Time == 'Future':
                        if self.FR.A_Words[0].Person == 1 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'arai'
                        if self.FR.A_Words[0].Person == 2 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'eras'
                        if self.FR.A_Words[0].Person > 2 and self.FR.A_Words[0].Amount == 'Singular':
                            suffix = 'era'
                        if self.FR.A_Words[0].Person == 1 and self.FR.A_Words[0].Amount == 'Plural':
                            suffix = 'erons'
                        if self.FR.A_Words[0].Person == 2 and self.FR.A_Words[0].Amount == 'Plural':
                            suffix = 'erez'

                if self.FR.A_Words[0].Part_Speech == 'Noun':
                    if temp.Time == 'Present':
                        suffix = 'e'
                    if temp.Time == 'Past':
                        suffix = 'a'
                    if temp.Time == 'Future':
                        suffix = 'era'

                temp.Suffix = suffix
                temp.Word = D.get_FR_Verb(temp.Dict_Index) + suffix
                temp.Part_Sentence = self.DE.A_Words[i].Part_Sentence
                self.FR.A_Words.append(temp)


