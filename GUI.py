from PySide.QtGui import *
from PySide.QtCore import *
import Translator
import Dictionary


Dict = Dictionary.Dictionary()

T = Translator.Translator()


class Diction(QWidget):
    def __init__(self):
        super(Diction, self).__init__()
        self.WidgetPrepositionDictionary = DictionaryPrepositionWindow()
        self.WidgetNounDictionary = DictionaryNounWindow()
        self.WidgetPronounDictionary = DictionaryPronounWindow()

        layout = QVBoxLayout(self)

        ButtonNoun = QPushButton('Словарь существительных')
        layout.addWidget(ButtonNoun)
        self.connect(ButtonNoun, SIGNAL('clicked()'), self.openNounDict)

        ButtonNoun = QPushButton('Словарь предлогов и союзов')
        layout.addWidget(ButtonNoun)
        self.connect(ButtonNoun, SIGNAL('clicked()'), self.openPrepositionDict)

        ButtonPronoun = QPushButton('Словарь местоимений')
        layout.addWidget(ButtonPronoun)
        self.connect(ButtonPronoun, SIGNAL('clicked()'), self.openPronounDict)

    def openNounDict(self):
        self.WidgetNounDictionary.setFixedSize(400, 500)
        self.WidgetNounDictionary.show()

    def openPrepositionDict(self):
        self.WidgetPrepositionDictionary.setFixedSize(350, 150)
        self.WidgetPrepositionDictionary.show()

    def openPronounDict(self):
        self.WidgetPronounDictionary.setFixedSize(350, 300)
        self.WidgetPronounDictionary.show()


class GUI(QWidget):
    def __init__(self):
        super(GUI, self).__init__()
        self.WidgetPrepositionDictionary = DictionaryPrepositionWindow()
        self.WidgetNounDictionary = DictionaryNounWindow()
        self.WidgetPronounDictionary = DictionaryPronounWindow()

        layout = QVBoxLayout(self)

        DE = QLabel('Deutsch:')
        layout.addWidget(DE)
        self.line = QTextEdit()
        self.line.setFixedSize(250, 50)
        layout.addWidget(self.line)

        button = QPushButton('Перевести')
        layout.addWidget(button)
        self.connect(button, SIGNAL('clicked()'), self.translate)

        English = QLabel('English:')
        layout.addWidget(English)
        self.EN_label = QTextBrowser()
        self.EN_label.setFixedSize(250, 50)
        layout.addWidget(self.EN_label)

        FR = QLabel('France:')
        layout.addWidget(FR)
        self.FR_label = QTextBrowser()
        self.FR_label.setFixedSize(250, 50)
        layout.addWidget(self.FR_label)

    def translate(self):
        T.set_text(self.line.toPlainText())
        T.translateDEtoEN()
        T.translateENtoFR()
        T.DE.show()
        self.EN_label.setText(T.get_text_EN())
        self.FR_label.setText(T.get_text_FR())
        self.line.setText(T.DE.get_text())

    def openNounDict(self):
        self.WidgetNounDictionary.setFixedSize(400, 500)
        self.WidgetNounDictionary.show()

    def openPrepositionDict(self):
        self.WidgetPrepositionDictionary.setFixedSize(350, 150)
        self.WidgetPrepositionDictionary.show()

    def openPronounDict(self):
        self.WidgetPronounDictionary.setFixedSize(350, 300)
        self.WidgetPronounDictionary.show()


class DictionaryNounWindow(QWidget):
    def __init__(self):
        super(DictionaryNounWindow, self).__init__()
        Window = QVBoxLayout(self)
        NounName = QLabel('<center><B><big>Nouns</big></B></center>')
        NounName.setFixedSize(400, 15)
        Nounw = Noun()
        Window.addWidget(NounName)
        Window.addWidget(Nounw)


class Noun(QWidget):
    def __init__(self):
        super(Noun, self).__init__()
        Nounw = QHBoxLayout(self)
        Statew = State()
        DEw = DE_Noun()
        ENw = EN_Noun()
        FRw = FR_Noun()
        Nounw.addWidget(Statew)
        Nounw.addWidget(DEw)
        Nounw.addWidget(ENw)
        Nounw.addWidget(FRw)


class State(QWidget):
    def __init__(self):
        super(State, self).__init__()
        statew = QVBoxLayout(self)
        state_Label = QLabel('<font color = "blue">State</font>')
        statew.addWidget(state_Label)
        for i in Dict.DE_Noun:
            state_Label = QLabel(i[1])
            statew.addWidget(state_Label)


class DE_Noun(QWidget):
    def __init__(self):
        super(DE_Noun, self).__init__()
        DEw = QVBoxLayout(self)
        DE_Label = QLabel('<font color = "blue">Deutsch</font>')
        DEw.addWidget(DE_Label)
        for i in Dict.DE_Noun:
            DE_Label = QLabel(i[0])
            DEw.addWidget(DE_Label)


class EN_Noun(QWidget):
    def __init__(self):
        super(EN_Noun, self).__init__()
        ENw = QVBoxLayout(self)
        EN_Label = QLabel('<font color = "blue">English:</font>')
        ENw.addWidget(EN_Label)
        for i in Dict.EN_Noun:
            EN_Label = QLabel(i[0])
            ENw.addWidget(EN_Label)


class FR_Noun(QWidget):
    def __init__(self):
        super(FR_Noun, self).__init__()
        FRw = QVBoxLayout(self)
        FR_Label = QLabel('<font color = "blue">France:</font>')
        FRw.addWidget(FR_Label)
        for i in Dict.FR_Noun:
            FR_Label = QLabel(i[0])
            FRw.addWidget(FR_Label)


class DictionaryPrepositionWindow(QWidget):
    def __init__(self):
        super(DictionaryPrepositionWindow, self).__init__()
        Window = QVBoxLayout(self)
        PrepositionName = QLabel('<center><B><big>Preposition</big></B></center>')
        PrepositionName.setFixedSize(350, 15)
        PrepositionWidget = Preposition()
        Window.addWidget(PrepositionName)
        Window.addWidget(PrepositionWidget)


class Preposition(QWidget):
    def __init__(self):
        super(Preposition, self).__init__()
        PrepositionW = QHBoxLayout(self)
        DEw = DE_Preposition()
        ENw = EN_Preposition()
        FRw = FR_Preposition()
        PrepositionW.addWidget(DEw)
        PrepositionW.addWidget(ENw)
        PrepositionW.addWidget(FRw)


class DE_Preposition(QWidget):
    def __init__(self):
        super(DE_Preposition, self).__init__()
        DEw = QVBoxLayout(self)
        DE_Label = QLabel('<font color = "blue">Deutch</font>')
        DEw.addWidget(DE_Label)
        for i in Dict.DE_Preposition:
            DE_Label = QLabel(i)
            DEw.addWidget(DE_Label)


class EN_Preposition(QWidget):
    def __init__(self):
        super(EN_Preposition, self).__init__()
        ENw = QVBoxLayout(self)
        EN_Label = QLabel('<font color = "blue">English:</font>')
        ENw.addWidget(EN_Label)
        for i in Dict.EN_Preposition:
            EN_Label = QLabel(i)
            ENw.addWidget(EN_Label)


class FR_Preposition(QWidget):
    def __init__(self):
        super(FR_Preposition, self).__init__()
        FRw = QVBoxLayout(self)
        FR_Label = QLabel('<font color = "blue">France:</font>')
        FRw.addWidget(FR_Label)
        for i in Dict.FR_Preposition:
            FR_Label = QLabel(i)
            FRw.addWidget(FR_Label)


class DictionaryPronounWindow(QWidget):
    def __init__(self):
        super(DictionaryPronounWindow, self).__init__()
        Window = QVBoxLayout(self)
        PronounName = QLabel('<center><B><big>Pronouns</big></B></center>')
        PronounName.setFixedSize(350, 15)
        PronounWidget = Pronoun()
        Window.addWidget(PronounName)
        Window.addWidget(PronounWidget)


class Pronoun(QWidget):
    def __init__(self):
        super(Pronoun, self).__init__()
        PronounW = QHBoxLayout(self)
        DEw = DE_Pronoun()
        ENw = EN_Pronoun()
        FRw = FR_Pronoun()
        PronounW.addWidget(DEw)
        PronounW.addWidget(ENw)
        PronounW.addWidget(FRw)


class DE_Pronoun(QWidget):
    def __init__(self):
        super(DE_Pronoun, self).__init__()
        DEw = QVBoxLayout(self)
        DE_Label = QLabel('<font color = "blue">Deutsch</font>')
        DEw.addWidget(DE_Label)
        for i in range(len(Dict.DE_Pronoun)):
            DE_Label = QLabel(Dict.get_DE_Pronoun(i+1, "Singular", 1))
            DEw.addWidget(DE_Label)

        for i in range(len(Dict.DE_Pronoun)):
            DE_Label = QLabel(Dict.get_DE_Pronoun(i+1, "Plural", 1))
            DEw.addWidget(DE_Label)


class EN_Pronoun(QWidget):
    def __init__(self):
        super(EN_Pronoun, self).__init__()
        ENw = QVBoxLayout(self)
        EN_Label = QLabel('<font color = "blue">English:</font>')
        ENw.addWidget(EN_Label)
        for i in range(len(Dict.EN_Pronoun)):
            EN_Label = QLabel(Dict.get_EN_Pronoun(i+1, 'Singular', 'Active', 1))
            ENw.addWidget(EN_Label)

        for i in range(len(Dict.EN_Pronoun)):
            EN_Label = QLabel(Dict.get_EN_Pronoun(i+1, 'Plural', 'Active', 1))
            ENw.addWidget(EN_Label)


class FR_Pronoun(QWidget):
    def __init__(self):
        super(FR_Pronoun, self).__init__()
        FRw = QVBoxLayout(self)
        FR_Label = QLabel('<font color = "blue">France:</font>')
        FRw.addWidget(FR_Label)
        for i in range(len(Dict.EN_Pronoun)):
            FR_Label = QLabel(Dict.get_FR_Pronoun(i+1, 'Singular', 1))
            FRw.addWidget(FR_Label)

        for i in range(len(Dict.EN_Pronoun)):
            FR_Label = QLabel(Dict.get_FR_Pronoun(i+1, 'Plural', 1))
            FRw.addWidget(FR_Label)


app = QApplication([])

w = GUI()
w.setWindowIconText('Translator')
w.setFixedSize(270, 270)
w.setWindowTitle('Translator')
w.show()

w1 = Diction()
w1.setWindowIconText('Dictionary')
w1.setFixedSize(250, 100)
w1.setWindowTitle('Dictionary')
w1.show()

app.exec_()

